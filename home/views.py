from django.shortcuts import render
import requests

# Create your views here.
def index(request):
    response = requests.get('https://api.kawalcorona.com/indonesia/')
    data = response.json()
    kasus_indonesia = {}
    kasus_indonesia['positif'] = data[0]['positif']
    kasus_indonesia['sembuh'] = data[0]['sembuh']
    kasus_indonesia['meninggal'] = data[0]['meninggal']
    kasus_indonesia['dirawat'] = data[0]['dirawat']
    return render(request, 'home/index.html', {
        "kasus_indonesia": kasus_indonesia
    })