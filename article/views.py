from django.http import request
from django.shortcuts import redirect, render
from .models import Article, Tags
from .forms import ArticleForm, TagsForm, ImageForm


def index(request):
    article = Article.objects.all()
    context = {'page_title': 'Article Corona Terbaru', 'article': article}
    return render(request, "article/index.html", context)


def detail(request, id):
    article = Article.objects.get(id=id)
    context = {'page_title': article.title, 'article': article}
    return render(request, "article/detail.html", context)


def tags(request, name):
    tags = Tags.objects.filter(name=name)
    article = map(lambda x: x.article, tags)
    context = {'page_title': name, 'article': article}
    return render(request, "article/tags.html", context)


def register(request):
    form_article = ArticleForm(request.POST or None)
    form_image = ImageForm(request.POST or None)
    if (request.method == 'POST'):
        if(form_article.is_valid()):
            form_article.save()
        if(form_image.is_valid()):
            form_image.save()
        return redirect('article:index')
    context =  {'page_title': 'register', 'form_article': form_article,
                'form_image': form_image}
    return render(request, "article/register.html", context)


def register_tags(request):
    form_tags = TagsForm(request.POST or None)
    if (request.method == 'POST' and form_tags.is_valid()):
        form_tags.save()
        return redirect('article:index')
    context = {'page_title': 'register','form_tags': form_tags}
    return render(request, "article/register_tags.html", context)
