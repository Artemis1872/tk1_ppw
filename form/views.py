from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.generic import CreateView

from .models import Patient


class UserCreateView(CreateView):
    model = Patient
    template_name = 'form/form.html'
    success_url = 'success'
    fields = (
        'name',
        'age',
        'sex',
        'job',
        'city',
        'province',
        'medical_history',
        'education',
        'symptoms',
    )

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

def success(request):
    return render(request, 'form/success.html')