from django.urls import path

from . import views
from form.views import UserCreateView

app_name = 'form'

urlpatterns = [
    path('', UserCreateView.as_view(), name='form'),
    path('success/', views.success, name='success'),
]
