from .models import RumahSakitRujukan, KasusProvinsi

def kasus_provinsi_helper(data):
    is_empty = KasusProvinsi.objects.count() == 0
    if is_empty:
        for objek in data:
            KasusProvinsi.objects.create(
                provinsi = objek["attributes"]["Provinsi"],
                kasus_positif = objek["attributes"]["Kasus_Posi"],
                kasus_sembuh = objek["attributes"]["Kasus_Semb"],
                kasus_meninggal = objek["attributes"]["Kasus_Meni"]
            )
    jakarta_object = KasusProvinsi.objects.get(provinsi = "DKI Jakarta")
    jakarta_object_json = data[0]["attributes"]
    is_latest = jakarta_object.kasus_positif == jakarta_object_json["Kasus_Posi"]
    if not is_latest:
        for objek in data:
            kasus_provinsi = KasusProvinsi.objects.get(
                provinsi = objek["attributes"]["Provinsi"]
            )
            kasus_provinsi.kasus_positif = objek["attributes"]["Kasus_Posi"]
            kasus_provinsi.kasus_sembuh = objek["attributes"]["Kasus_Semb"]
            kasus_provinsi.kasus_meninggal = objek["attributes"]["Kasus_Meni"]
            kasus_provinsi.save()

def rumah_sakit_helper(data):
    is_empty = RumahSakitRujukan.objects.count() == 0
    if is_empty:
        for objek in data:
            RumahSakitRujukan.objects.create(
                rumah_sakit = objek["name"],
                alamat = objek["address"],
                region = objek["region"],
                nomor_telepon = objek["phone"],
                provinsi = objek["province"]
            )
    is_latest = RumahSakitRujukan.objects.count() == len(data)
    if not is_latest:
        for objek in data:
            if not RumahSakitRujukan.objects.filter(rumah_sakit = objek["name"]).exists():
                RumahSakitRujukan.objects.create(
                    rumah_sakit = objek["name"],
                    alamat = objek["address"],
                    region = objek["region"],
                    nomor_telepon = objek["phone"],
                    provinsi = objek["province"]
                )
                 